package br.senai.sp.informatica.findfilm.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import java.util.List;

import br.senai.sp.informatica.findfilm.R;
import br.senai.sp.informatica.findfilm.model.Genero;

/**
 * Created by Tecnico_Tarde on 11/05/2017.
 */

public class RVGenerosAdapter extends RecyclerView.Adapter<RVGenerosAdapter.GenerosViewHolder> {

    private final List<Genero> generos;
    private final Context context;
    private final GenerosOnClickListener onClickListener;

    public RVGenerosAdapter(List<Genero> generos, Context context,GenerosOnClickListener onClickListener){
        this.generos = generos;
        this.context = context;
        this.onClickListener = onClickListener;
    }

    //interface de listener
    public interface GenerosOnClickListener{
        public void onClickGenero(View view,int position);
    }


    @Override
    public RVGenerosAdapter.GenerosViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
      //cria uma view
        // e infla nela o layout do adaptador
        View view = LayoutInflater.from(context).inflate(R.layout.adapter_generos, parent,false);
        return new GenerosViewHolder(view);


    }

    @Override
    public void onBindViewHolder(RVGenerosAdapter.GenerosViewHolder holder,final int position) {

        //o nome do final nao vai ser mais constante vai ser INVARIAVEL PQ EU QUERO
        holder.nomeGenero.setText(generos.get(position).getNome());
        holder.ratingClassificacao.setRating(generos.get(position).getClassificacao());

        //se o listener de click for diferente de null
        if (onClickListener!=null){
            //obtem o widget que foi clicado e executa seu listener
            holder.itemView.setOnClickListener(new View.OnClickListener(){
                public void onClick(View v){
                    onClickListener.onClickGenero(v,position);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return generos != null ?generos.size() : 0;
    }

    //class de view holder faz o fingView byid dos bang do adapter
    public class GenerosViewHolder extends  RecyclerView.ViewHolder{
        public TextView nomeGenero;
        public RatingBar ratingClassificacao;
        private View itemView;


        public GenerosViewHolder(View itemView){
            super(itemView);
            //faz o findViewByid
            this.itemView = itemView;

            nomeGenero = (TextView) itemView.findViewById(R.id.nome_genero_adapter);

            ratingClassificacao = (RatingBar) itemView.findViewById(R.id.rating_class_genero_adapter);
        }
    }
}
