package br.senai.sp.informatica.findfilm.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import java.util.List;

import br.senai.sp.informatica.findfilm.R;
import br.senai.sp.informatica.findfilm.model.Diretor;

/**
 * Created by Tecnico_Tarde on 27/04/2017.
 */

public class RVDiretoresAdapter extends RecyclerView.Adapter<RVDiretoresAdapter.DiretoresViewHolder> {

    private final List<Diretor> diretores;
    private final Context context;
    private final DiretorOnClickListener onClickListener;

    public RVDiretoresAdapter(List<Diretor> diretores, Context context, DiretorOnClickListener onClickListener)  {
        this.diretores = diretores;
        this.context = context;
        this.onClickListener = onClickListener;
    }


    //interface de listener
    public interface DiretorOnClickListener {
        public void onClickDiretor(View view, int position);

        
    }


    @Override
    public RVDiretoresAdapter.DiretoresViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //cria uma view
        // e infla nela o layout do adaptador
        View view = LayoutInflater.from(context).inflate(R.layout.adapter_diretores, parent, false);
        return new DiretoresViewHolder(view);

    }

    @Override
    public void onBindViewHolder(RVDiretoresAdapter.DiretoresViewHolder holder, final int position) {

        //associa os widgets do adaptador aos dados do  objeto

        //associa o nome do diretor ao textView
        holder.nomeDiretor.setText(diretores.get(position).getNome());
        //verifica se o diretor tem foto
        if (diretores.get(position).getFoto() != null) {
            //cria um bitmap
            Bitmap bitmap = BitmapFactory.decodeFile(diretores.get(position).getFoto());
            //associa a foto ao imageview
            holder.fotoDiretor.setImageBitmap(bitmap);
        } else {
            //no image
            holder.fotoDiretor.setImageResource(R.drawable.noimage);
        }

        //associa o ratingbar
        holder.ratingClassificacao.setRating(diretores.get(position).getClassificacao());

        //determina o comportamento do listener de click

        //se o listener de click for diferente de null
        if(onClickListener!=null){
            //obtem o widget que foi clicado e executa seu listene
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClickListener.onClickDiretor(v,position);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return diretores != null ? diretores.size() : 0;
    }


    //class de view holder faz o dingview byid dos widgets do adapter
    public class DiretoresViewHolder extends RecyclerView.ViewHolder {

        public TextView nomeDiretor;
        public ImageView fotoDiretor;
        public RatingBar ratingClassificacao;
        private View itemView;

        public DiretoresViewHolder(View itemView) {
            super(itemView);
            //faz o findViewById
            this.itemView = itemView;
            nomeDiretor = (TextView) itemView.findViewById(R.id.nome_diretor_adapter);

            fotoDiretor = (ImageView) itemView.findViewById(R.id.foto_diretor_adapter);

            ratingClassificacao = (RatingBar) itemView.findViewById(R.id.rating_class_diretor_adapter);


        }
    }
}
