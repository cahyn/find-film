package br.senai.sp.informatica.findfilm.model;

import org.parceler.Parcel;

import java.util.Calendar;

/**
 * Created by Tecnico_Tarde on 15/05/2017.
 */

@Parcel
public class Filme {
    private Long id;
    private String titulo;
    private String sinopse;
    private ClassificacaoIndicativa classificacaoIndicativa;
    private int tempoDuracao;
    private Calendar dataLancamento;
    private String capa;
    private Genero genero;
    private Diretor diretor;
    private float nota;


    public Filme() {
    }

    public Filme(Long id, String titulo, String sinopse, ClassificacaoIndicativa classificacaoIndicativa, int tempoDuracao, Calendar dataLancamento, String capa, Genero genero, Diretor diretor, float nota) {
        this.id = id;
        this.titulo = titulo;
        this.sinopse = sinopse;
        this.classificacaoIndicativa = classificacaoIndicativa;
        this.tempoDuracao = tempoDuracao;
        this.dataLancamento = dataLancamento;
        this.capa = capa;
        this.genero = genero;
        this.diretor = diretor;
        this.nota = nota;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getSinopse() {
        return sinopse;
    }

    public void setSinopse(String sinopse) {
        this.sinopse = sinopse;
    }

    public ClassificacaoIndicativa getClassificacaoIndicativa() {
        return classificacaoIndicativa;
    }

    public void setClassificacaoIndicativa(ClassificacaoIndicativa classificacaoIndicativa) {
        this.classificacaoIndicativa = classificacaoIndicativa;
    }

    public int getTempoDuracao() {
        return tempoDuracao;
    }

    public void setTempoDuracao(int tempoDuracao) {
        this.tempoDuracao = tempoDuracao;
    }

    public Calendar getDataLancamento() {
        return dataLancamento;
    }

    public void setDataLancamento(Calendar dataLancamento) {
        this.dataLancamento = dataLancamento;
    }

    public String getCapa() {
        return capa;
    }

    public void setCapa(String capa) {
        this.capa = capa;
    }

    public Genero getGenero() {
        return genero;
    }

    public void setGenero(Genero genero) {
        this.genero = genero;
    }

    public Diretor getDiretor() {
        return diretor;
    }

    public void setDiretor(Diretor diretor) {
        this.diretor = diretor;
    }

    public float getNota() {
        return nota;
    }

    public void setNota(float nota) {
        this.nota = nota;
    }
}
