package br.senai.sp.informatica.findfilm.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import java.util.List;

import br.senai.sp.informatica.findfilm.R;
import br.senai.sp.informatica.findfilm.model.ClassificacaoIndicativa;
import br.senai.sp.informatica.findfilm.model.Filme;

/**
 * Created by Tecnico_Tarde on 22/05/2017.
 */

public class RVFilmesAdapter extends  RecyclerView.Adapter<RVFilmesAdapter.FilmesViewHoldes> {

    private final List<Filme> filmes;
    private final Context context;
    private final FilmesOnClickListener onClickListener;

    public interface  FilmesOnClickListener{
        public void OnClickFilme(View view,int position);
    }
    //bob o construtor
    public  RVFilmesAdapter(Context context, List<Filme> filmes, FilmesOnClickListener onClickListener){
        this.context = context;
        this.filmes = filmes;
        this.onClickListener = onClickListener;

    }

    @Override
    public FilmesViewHoldes onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.adapter_filmes,parent,false);
        //retorna uma instancia de filmesViewHolder
        return new FilmesViewHoldes(view);
    }

    @Override
    public void onBindViewHolder(FilmesViewHoldes holder, final int position) {
        //capa
        if (filmes.get(position).getCapa() != null){
            Bitmap bitmap = BitmapFactory.decodeFile(filmes.get(position).getCapa());
            holder.imgCapa.setImageBitmap(bitmap);
        }else {
            holder.imgCapa.setImageResource(R.drawable.noimage);
        }
        holder.textTitulo.setText(filmes.get(position).getTitulo());
        holder.textGenero.setText(filmes.get(position).getGenero().getNome());
        holder.ratingNota.setRating(filmes.get(position).getNota());

        //class. indicativa
        String classInd = filmes.get(position).getClassificacaoIndicativa().nome;
        if (ClassificacaoIndicativa.LIVRE.nome.equals(classInd)){
            holder.imgClassInd.setImageResource(R.drawable.classificacao_livre);
        }else  if(ClassificacaoIndicativa.DEZ_ANOS.nome.equals(classInd)){
            holder.imgClassInd.setImageResource(R.drawable.dezanos);
        }else if(ClassificacaoIndicativa.DOZE_ANOS.nome.equals(classInd)){
            holder.imgClassInd.setImageResource(R.drawable.dozeanos);
        }else if(ClassificacaoIndicativa.QUATORZE_ANOS.nome.equals(classInd)){
        holder.imgClassInd.setImageResource(R.drawable.quatorzeanos);
        }else if(ClassificacaoIndicativa.DEZESSEIS_ANOS.nome.equals(classInd)){
            holder.imgClassInd.setImageResource(R.drawable.dezesseisanos);
        }else{
        holder.imgClassInd.setImageResource(R.drawable.dezoitoanos);
         }
         // listener  SEM ISSO NÂO FUNCIONA!
         if (onClickListener != null){
             holder.itemView.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View v) {
                     // redireciona o evento para o onClickFilme

                     onClickListener.OnClickFilme(v , position);
                 }
             });
         }
        }

    @Override
    public int getItemCount() {
        return filmes != null ? filmes.size() : 0;
    }

    //classe de view HOLDER
    //serve para fazer o findviewByid
    //dos widgets presentes no layuout do adaptador /layout/adapter_filmes.xml

    public class FilmesViewHoldes extends RecyclerView.ViewHolder{
        ImageView imgCapa;
        TextView textTitulo;
        TextView textGenero;
        RatingBar ratingNota;
        ImageView imgClassInd;

        public FilmesViewHoldes(View itemView){
            super(itemView);
            //faz o findviewByid da porra toda
            imgCapa = (ImageView) itemView.findViewById(R.id.capa_filme_adapter);
            textTitulo = (TextView) itemView.findViewById(R.id.titulo_filme_adapter);
            textGenero = (TextView) itemView.findViewById(R.id.genero_filme_adapter);
            ratingNota = (RatingBar) itemView.findViewById(R.id.nota_filme_adapter);
            imgClassInd = (ImageView) itemView.findViewById(R.id.class_ind_adapter);
        }

    }
}
