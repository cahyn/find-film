package br.senai.sp.informatica.findfilm.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.Toast;

import org.parceler.Parcels;

import java.io.File;

import br.senai.sp.informatica.findfilm.R;
import br.senai.sp.informatica.findfilm.dao.DiretorDao;
import br.senai.sp.informatica.findfilm.model.Diretor;
import br.senai.sp.informatica.findfilm.util.MessageUtils;
import br.senai.sp.informatica.findfilm.util.PermissionUtils;

public class SaveDiretoresActivity extends BaseActivity {


    EditText editNomeDiretor;
    ImageView imageFotoDiretor;
    RatingBar ratingClasDiretor;
    CoordinatorLayout diretoresLayout;



    public static final int CAMERA_REQUEST_CODE = 1;
    public static final int GALERA_REQUEST_CODE = 2;

    String[] permissoes = new String[]{
            Manifest.permission.CAMERA,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    File photoFile;


    Diretor diretor = null;
    DiretorDao diretorDao = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_save_diretores);
        inicializarComponentes();
        setupToolbar();

        //recupera os extras
        //verifica se há extras
        if (getIntent().getExtras() != null){
            //se vieram extras
            //obter o diretor
            diretor = Parcels.unwrap(getIntent().getParcelableExtra("diretor"));
            if (diretor != null){
                editNomeDiretor.setText(diretor.getNome());

                //foto
                Bitmap bitmap = BitmapFactory.decodeFile(diretor.getFoto());

                if(bitmap !=null){
                    imageFotoDiretor.setImageBitmap(bitmap);
                }else{
                    imageFotoDiretor.setImageResource(R.drawable.noimage);
                }
                ratingClasDiretor.setRating(diretor.getClassificacao());

            }
        }


    }

    @Override
    public void inicializarComponentes() {

    }

    @Override
    public void setupToolbar() {
        super.setupToolbar();
        editNomeDiretor = (EditText) findViewById(R.id.edit_diretor);
        imageFotoDiretor = (ImageView) findViewById(R.id.foto_diretor);
        ratingClasDiretor = (RatingBar) findViewById(R.id.rating_nota_diretor);

        diretoresLayout = (CoordinatorLayout) findViewById(R.id.diretores_layout);
    }

    public void setFotoDiretor(View view){
        //exibe um diálogo para o usuário selecionar a origem da foto
        fotoDialog().show();
    }
    private AlertDialog fotoDialog(){
        CharSequence[] opcoes = new CharSequence[] {
                getString(R.string.camera),
                getString(R.string.galeria)

        };
        //construir o alertDialog
        //cria uma instancia de builder
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle(getString(R.string.opcoes_imagem));
        builder.setItems(opcoes,onImageSelectListener());
        builder.setIcon(android.R.drawable.ic_menu_camera);
        //retorna a criação do alertDialog
        return builder.create();
    }

    //listener do aLErtDialog
    private DialogInterface.OnClickListener onImageSelectListener(){
        return new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                switch (which){
                    case 0:
                        //camera

                        dispatcherTakePicture();
                        break;

                    case 1:
                if(PermissionUtils.isValidate(SaveDiretoresActivity.this, GALERA_REQUEST_CODE,permissoes));
                        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(intent,GALERA_REQUEST_CODE);
                }
            }
        };
    }

    private void dispatcherTakePicture(){
        //verifica as permissões
        if(PermissionUtils.isValidate(this, CAMERA_REQUEST_CODE,permissoes)){
            //intent para acessar a camera
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

            //determina a melhor forma executar a intent
            if(takePictureIntent.resolveActivity(getPackageManager()) != null){
                //prepara o arquivo de foto
             photoFile = null;
             photoFile = createImage();
                if(photoFile != null){
                    //determina o caminho para salvar a foto
                    Uri uri = FileProvider.getUriForFile(getBaseContext(),"com.example.android.fileprovider",photoFile);
                    //inicia a activity para exibiar a foto no ImageView
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
                    startActivityForResult(takePictureIntent, CAMERA_REQUEST_CODE);
                }
            }
        }
    }

    private File createImage(){
        String name = System.currentTimeMillis() + ".jpg" ;

    File diretorio = getExternalFilesDir(Environment.DIRECTORY_PICTURES);

        String caminho = diretorio.getPath() + "/" + name;

        File imageFile = new File(caminho);
        return  imageFile;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        Bitmap bitmap = null;
        if(requestCode == CAMERA_REQUEST_CODE && resultCode == RESULT_OK){
            bitmap = BitmapFactory.decodeFile(photoFile.getAbsolutePath());
        }
        if(requestCode == GALERA_REQUEST_CODE && resultCode == RESULT_OK){
            if(data !=null){
                Uri uri = data.getData();
                // Nome do diretorio ond está a imagem
                String [] filePathColumn = {MediaStore.Images.Media.DATA};
                Cursor cursor = getContentResolver().query(uri,filePathColumn,null,null,null);

                //move o cursor para a primeira posição
                cursor.moveToFirst();

                //obtem o id da coljuna  0 (caminho da imagem)

                int columIndex = cursor.getColumnIndex(filePathColumn[0]);

                String caminho = cursor.getString(columIndex);

                //fecha o cursor

                cursor.close();

                //cria um File com o caminho selecionado
                photoFile = new File(caminho);
                bitmap = BitmapFactory.decodeFile(photoFile.getAbsolutePath());

            }
        }
        imageFotoDiretor.setImageBitmap(bitmap);


    }


    public void saveDiretorOnClick(View view){
        if(isValidated()) {
            try {
                if (diretor == null) {
                    diretor = new Diretor();
                }
                //atribui os dados ao diretor
                diretor.setNome(editNomeDiretor.getEditableText().toString());

                diretor.setClassificacao(ratingClasDiretor.getRating());

                if (photoFile != null) {
                    diretor.setFoto(photoFile.getAbsolutePath().toString());
                }
                diretorDao = new DiretorDao(this);


                if (diretorDao.save(diretor) != -1) {
                    //sucesso
                    MessageUtils.snack(diretoresLayout, getString(R.string.sucesso_salvar_diretores));
                    clear();
                } else {
                    //erro
                    MessageUtils.snack(diretoresLayout, getString(R.string.erro_salvar_diretores));
                }

            }catch (SQLiteConstraintException e){
                MessageUtils.snack(diretoresLayout,getString(R.string.diretor_cadastrado));
            }

        }
    }

    private void clear(){
        editNomeDiretor.setText("");
        imageFotoDiretor.setImageResource(R.drawable.noimage);
        ratingClasDiretor.setRating(0);
        editNomeDiretor.requestFocus();
        diretor = null;
    }


    private boolean isValidated(){
        if(editNomeDiretor.getEditableText().toString().isEmpty()) {
            MessageUtils.snack(diretoresLayout, getString(R.string.preencha_nome_diretor));
            editNomeDiretor.requestFocus();
            return false;
        }else if (ratingClasDiretor.getRating() < 0.5){
            MessageUtils.snack(diretoresLayout, getString(R.string.preencha_classificacao_diretor));
           return false;
        }
        return true;



        }



    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.d("TAG","onSaveInstanceState()");
    outState.putString("nome",editNomeDiretor.getEditableText().toString());
        if(photoFile != null){
        outState.putString("foto",photoFile.getAbsolutePath());
            }
            outState.putFloat("classificacao",ratingClasDiretor.getRating());
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        if (savedInstanceState != null) {

            //cria um objeto diretor
            diretor = new Diretor();

            //define os atributos de diretor
            //recuperando os valores da aplicaçãpo

            diretor.setNome(savedInstanceState.getString("nome"));
            diretor.setFoto(savedInstanceState.getString("foto"));
            diretor.setClassificacao(savedInstanceState.getFloat("classificacao"));

            editNomeDiretor.setText(diretor.getNome());

            //foto
            if (diretor.getFoto() != null) {
                photoFile = new File(diretor.getFoto());

                //bitmap
                Bitmap bitmap = BitmapFactory.decodeFile(photoFile.getAbsolutePath());

                if (bitmap != null) {
                    imageFotoDiretor.setImageBitmap(bitmap);
                } else {
                    imageFotoDiretor.setImageResource(R.drawable.noimage);

                }
                ratingClasDiretor.setRating(diretor.getClassificacao());
            }
        }
    }
}





