package br.senai.sp.informatica.findfilm.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import br.senai.sp.informatica.findfilm.model.Diretor;

/**
 * Created by Tecnico_Tarde on 17/04/2017.
 */

public class DiretorDao implements DefaultDao<Diretor> {

    //atributos
    private SQLiteDatabase sqLiteDatabase;
    private DBHelper dbHelper;
    private Context context;

    // Bob Construtor
    public DiretorDao(Context context) {
        this.context = context;
        this.dbHelper = new DBHelper(context);
    }

    @Override
    public long save(Diretor diretor) {
        if (sqLiteDatabase == null) {
            //se assim for cria "uma conexão com banco de dados
            //abre o arquvio de banco de dados com
            // permissões de escrita
            sqLiteDatabase = dbHelper.getWritableDatabase();
        }
        try {
            ContentValues values = new ContentValues();
            //associa os valores do objeto diretor as colunas do banco de dados
            values.put("nome", diretor.getNome());
            values.put("foto", diretor.getFoto());
            values.put("classificacao", diretor.getClassificacao());

            if (diretor.getId() != 0) {
                //update
                String[] whereArgs = new String[]{
                        String.valueOf(diretor.getId())
                };

                return sqLiteDatabase.update("diretor", values, "id=?", whereArgs);
            } else {
                //insert
                return sqLiteDatabase.insertOrThrow("diretor", null, values);

            }

        } finally {
            //fecha o arquivo do banco de dados
            sqLiteDatabase.close();
        }

    }

    @Override
    public List<Diretor> findAll() {
       if(sqLiteDatabase == null){
           sqLiteDatabase = dbHelper.getWritableDatabase();
       }
       try{
           String sql = "SELECT * FROM diretor ORDER BY classificacao DESC";

           Cursor cursor = sqLiteDatabase.rawQuery(sql,null);
           return toList(cursor);
       }finally {
           sqLiteDatabase.close();
       }
    }

    @Override
    public List<Diretor> find(String query) {
        if (sqLiteDatabase ==null){
            sqLiteDatabase = dbHelper.getWritableDatabase();
            }
        try {
            //parametro where
            String[] parametro =
                    new String[]{"%" + query + "%"}  ;
            //sql
            String sql = "SELECT * FROM diretor WHERE nome LIKE ? ORDER BY classificacao DESC";
            Cursor  cursor = sqLiteDatabase.rawQuery(sql,parametro);

            //retorna a lista com os diretores
            return toList(cursor);


        }finally {
            sqLiteDatabase.close();
        }

    }

    @Override
    public long delete(Diretor diretor) {
        //verifica se o arquivo de banco de dados não esta aberto ou se é nulo

        if(!sqLiteDatabase.isOpen() || sqLiteDatabase ==null){
        sqLiteDatabase = dbHelper.getWritableDatabase();
        }
        try{
            String[] whereArgs = new String[] {String.valueOf(diretor.getId())
            };
            return sqLiteDatabase.delete("diretor", "id=?",whereArgs);
        }finally {
            sqLiteDatabase.close();
        }


    }

    @Override
    public List<Diretor> toList(Cursor cursor) {
        List<Diretor> diretores = new ArrayList<>();

        //se houver algum diretor cadastrado
        if (cursor.moveToFirst()) {
            do {
                //repete para todos os registros
                Diretor diretor = new Diretor();
                //se der erro pode ser pq aqui é Long
                diretor.setId(cursor.getLong(cursor.getColumnIndex("id")));
                diretor.setNome(cursor.getString(cursor.getColumnIndex("nome")));
                diretor.setClassificacao(cursor.getFloat(cursor.getColumnIndex("classificacao")));
                diretor.setFoto(cursor.getString(cursor.getColumnIndex("foto")));

                // adiciona o diretor á lista
                diretores.add(diretor);

            } while (cursor.moveToNext());

        }
        return diretores;

    }
}
