package br.senai.sp.informatica.findfilm.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import br.senai.sp.informatica.findfilm.model.ClassificacaoIndicativa;
import br.senai.sp.informatica.findfilm.model.Diretor;
import br.senai.sp.informatica.findfilm.model.Filme;
import br.senai.sp.informatica.findfilm.model.Genero;

/**
 * Created by Tecnico_Tarde on 15/05/2017.
 */

public class FilmeDao implements DefaultDao<Filme> {

    private SQLiteDatabase sqLiteDatabase;
    private DBHelper dbHelper;
    private Context context;

    //construtor
    public FilmeDao(Context context){
        this.context = context;
        this.dbHelper = new DBHelper(context);
    }



    @Override
    public long save(Filme filme) {
        if (sqLiteDatabase ==null){
            sqLiteDatabase = dbHelper.getWritableDatabase();
        }
        try{
            ContentValues values = new ContentValues();
            values.put("titulo",filme.getTitulo());
            values.put("sinopse",filme.getSinopse());
            values.put("duracao",filme.getTempoDuracao());
            values.put("classificacaoIndicativa", filme.getClassificacaoIndicativa().nome);
            values.put("capa",filme.getCapa());
            values.put("dataLancamento", filme.getDataLancamento().getTimeInMillis());
            values.put("genero_id", filme.getGenero().getId());
            values.put("diretor_id",filme.getDiretor().getId());
            values.put("nota",filme.getNota());
            if (filme.getId() != null){
                //update
                String [] whereArgs = new String[]{String.valueOf(filme.getId())};
                return sqLiteDatabase.update("filme",values, "id=?", whereArgs);
            }else{
                return  sqLiteDatabase.insertOrThrow("filme",null,values);
            }

        }finally {
            sqLiteDatabase.close();
        }

    }

    @Override
    public List<Filme> findAll() {
        if(sqLiteDatabase == null){
            sqLiteDatabase = dbHelper.getWritableDatabase();
        }
        try{
            String sql = "SELECT  F.id AS filme_id, F.titulo, F.sinopse, F.duracao, F.classificacaoIndicativa,F.capa, F.dataLancamento, F.genero_id, F.diretor_id , F.nota , G.id AS id_genero, G.nome AS nome_genero, G.classificacao AS class_genero, D.id AS id_diretor, D.nome AS nome_diretor, D.foto AS foto_diretor, D.classificacao AS class_diretor FROM  filme AS F JOIn genero AS G ON F.genero_id = G.id Join diretor AS D ON F.diretor_id = D.id;";

            Cursor cursor = sqLiteDatabase.rawQuery(sql, null);
            return toList(cursor);
        }finally {
            sqLiteDatabase.close();
        }
    }

    @Override
    public List<Filme> find(String query) {
        if (sqLiteDatabase ==null){
            sqLiteDatabase = dbHelper.getWritableDatabase();
        }
        try {
            //parametro where
            String[] parametro =
                    new String[]{"%" + query + "%"}  ;
            //sql
          //  String sql = "SELECT * FROM filme Join genero filme.genero_id=genero.id WHERE titulo LIKE ? ORDER BY genero.classificacao DESC";
            String sql = "SELECT  F.id AS filme_id, F.titulo, F.sinopse, F.duracao, F.classificacaoIndicativa,F.capa, F.dataLancamento, F.genero_id, F.diretor_id , F.nota , G.id AS id_genero, G.nome AS nome_genero, G.classificacao AS class_genero, D.id AS id_diretor, D.nome AS nome_diretor, D.foto AS foto_diretor, D.classificacao AS class_diretor FROM  filme AS F JOIn genero AS G ON F.genero_id = G.id Join diretor AS D ON F.diretor_id = D.id WHERE titulo LIKE ? ORDER BY F.nota DESC;";

            Cursor  cursor = sqLiteDatabase.rawQuery(sql,parametro);

            //retorna a lista com os diretores
            return toList(cursor);


        }finally {
            sqLiteDatabase.close();
        }
    }

    @Override
    public long delete(Filme filme) {
        if(!sqLiteDatabase.isOpen() || sqLiteDatabase ==null){
            sqLiteDatabase = dbHelper.getWritableDatabase();
        }
        try{
            String[] whereArgs = new String[] {String.valueOf(filme.getId())
            };
            return sqLiteDatabase.delete("filme", "id=?",whereArgs);
        }finally {
            sqLiteDatabase.close();
        }
    }

    @Override
    public List<Filme> toList(Cursor cursor) {
        List<Filme> filmes = new ArrayList<>();
        if (cursor.moveToFirst()){
            do {
                Filme filme = new Filme();
                filme.setId(cursor.getLong(cursor.getColumnIndex("filme_id")));
                filme.setTitulo(cursor.getString(cursor.getColumnIndex("titulo")));
                filme.setSinopse(cursor.getString(cursor.getColumnIndex("sinopse")));
                filme.setTempoDuracao(cursor.getInt(cursor.getColumnIndex("duracao")));

                //classficacao indicativa
                String classInd = cursor.getString(cursor.getColumnIndex("classificacaoIndicativa"));
                for(ClassificacaoIndicativa c : ClassificacaoIndicativa.values()){
                    if (c.nome.equals(classInd)){
                        filme.setClassificacaoIndicativa(c);
                        break;
                    }
                }
                filme.setCapa(cursor.getString(cursor.getColumnIndex("capa")));

                //data de Lançamento
                Calendar dataLancamento = Calendar.getInstance();
                Date date = new Date(cursor.getLong(cursor.getColumnIndex("dataLancamento")));
                dataLancamento.setTime(date);
                filme.setDataLancamento(dataLancamento);

                //composição de genero
                Genero genero = new Genero();
                genero.setId(cursor.getLong(cursor.getColumnIndex("id_genero")));
                genero.setNome(cursor.getString(cursor.getColumnIndex("nome_genero")));
                genero.setClassificacao(cursor.getFloat(cursor.getColumnIndex("class_genero")));
                filme.setGenero(genero);

                //composição do diretor
                Diretor diretor = new Diretor();
                diretor.setId(cursor.getLong(cursor.getColumnIndex("id_diretor")));
                diretor.setNome(cursor.getString(cursor.getColumnIndex("nome_diretor")));
                diretor.setFoto(cursor.getString(cursor.getColumnIndex("foto_diretor")));
                diretor.setClassificacao(cursor.getFloat(cursor.getColumnIndex("class_diretor")));
                filme.setDiretor(diretor);

                filme.setNota(cursor.getFloat(cursor.getColumnIndex("nota")));

                filmes.add(filme);


            }while (cursor.moveToNext());
        }
        return filmes;
    }
}
