package br.senai.sp.informatica.findfilm.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.TextInputEditText;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.TextView;

import org.parceler.Parcels;
import org.w3c.dom.Text;

import java.io.File;
import java.util.Calendar;
import java.util.Locale;

import br.senai.sp.informatica.findfilm.R;
import br.senai.sp.informatica.findfilm.dao.DiretorDao;
import br.senai.sp.informatica.findfilm.dao.FilmeDao;
import br.senai.sp.informatica.findfilm.dao.GeneroDao;
import br.senai.sp.informatica.findfilm.model.ClassificacaoIndicativa;
import br.senai.sp.informatica.findfilm.model.Diretor;
import br.senai.sp.informatica.findfilm.model.Filme;
import br.senai.sp.informatica.findfilm.model.Genero;
import br.senai.sp.informatica.findfilm.util.MessageUtils;
import br.senai.sp.informatica.findfilm.util.PermissionUtils;


public class SaveFilmeActivity extends BaseActivity {


    public static final int CAMERA_REQUEST_CODE = 1;
    public static final int GALERA_REQUEST_CODE = 2;



    String[] permissoes = new String[]{
            Manifest.permission.CAMERA,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    File photoFile;

    Spinner spinnerClassInd;
    ImageView imageClassInd;

    //vetor com as imagens de classificação indicativa
    int[] classIndArray = new int[]{
            R.drawable.classificacao_livre,
            R.drawable.dezanos,
            R.drawable.dozeanos,
            R.drawable.quatorzeanos,
            R.drawable.dezesseisanos,
            R.drawable.dezoitoanos
    };


    //data de lançamento
    int dia, mes, ano;
    TextView textDataLancamento;
    Locale locale = new Locale("pt", "BR");

    //capa do filme
    ImageView imgCapaFilme;


    Spinner spinnerGeneros;
    Spinner spinnerDiretores;

    TextInputEditText editTitulo;
    TextInputEditText editSinopse;
    TextInputEditText editDuracao;
    RatingBar ratingNota;

    Filme filme;

    FilmeDao filmeDao = new FilmeDao(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_save_filme);
        setupToolbar();
        inicializarComponentes();

        if (getIntent().getExtras() != null){
            //se vieram extras
            //obter o diretor
            filme = Parcels.unwrap(getIntent().getParcelableExtra("diretor"));
            if (filme != null){

                editTitulo.setText(filme.getTitulo());
                editSinopse.setText(filme.getSinopse());
                editDuracao.setText(filme.getTempoDuracao());
                ratingNota.setRating(filme.getNota());

                if (ClassificacaoIndicativa.LIVRE.nome.equals(filme.getClassificacaoIndicativa())){
                    imageClassInd.setImageResource(R.drawable.classificacao_livre);
                    spinnerClassInd.setSelection(1);
                }else  if(ClassificacaoIndicativa.DEZ_ANOS.nome.equals(filme.getClassificacaoIndicativa())){
                    imageClassInd.setImageResource(R.drawable.dezanos);
                    spinnerClassInd.setSelection(2);
                }else if(ClassificacaoIndicativa.DOZE_ANOS.nome.equals(filme.getClassificacaoIndicativa())){
                    imageClassInd.setImageResource(R.drawable.dozeanos);
                    spinnerClassInd.setSelection(3);
                }else if(ClassificacaoIndicativa.QUATORZE_ANOS.nome.equals(filme.getClassificacaoIndicativa())){
                    imageClassInd.setImageResource(R.drawable.quatorzeanos);
                    spinnerClassInd.setSelection(4);
                }else if(ClassificacaoIndicativa.DEZESSEIS_ANOS.nome.equals(filme.getClassificacaoIndicativa())){
                    imageClassInd.setImageResource(R.drawable.dezesseisanos);
                    spinnerClassInd.setSelection(5);
                }else{
                    imageClassInd.setImageResource(R.drawable.dezoitoanos);
                    spinnerClassInd.setSelection(6);

                }

                //foto
                Bitmap bitmap = BitmapFactory.decodeFile(filme.getCapa());

                if(bitmap !=null){
                    imgCapaFilme.setImageBitmap(bitmap);
                }else{
                    imgCapaFilme.setImageResource(R.drawable.noimage);
                }


            }
        }
    }

    public void inicializarComponentes() {
        //classificacao indicativa
        setupSpinnerClassInd();
        //data de lançamento
        setupSpinnerData();

        imgCapaFilme = (ImageView) findViewById(R.id.capa_filme_img);
        //spinner de genero
        setupSpinnerGeneros();

        //spiner de diretores
        setupSpinnerDiretores();

        editTitulo = (TextInputEditText) findViewById(R.id.filme_titulo_input);

        editSinopse = (TextInputEditText) findViewById(R.id.filme_sinopse_input);

        editDuracao = (TextInputEditText) findViewById(R.id.filme_duracao);

        ratingNota = (RatingBar) findViewById(R.id.filme_nota);

    }

    private void setupSpinnerClassInd() {
        spinnerClassInd = (Spinner) findViewById(R.id.classind_spinner);
        imageClassInd = (ImageView) findViewById(R.id.classind_img);

        //cria um adaptador de dados para o spinner
        ArrayAdapter<ClassificacaoIndicativa> adaptador = new ArrayAdapter<ClassificacaoIndicativa>(this, android.R.layout.simple_spinner_dropdown_item, ClassificacaoIndicativa.values());

        //associa o spiner ao adaptador
        spinnerClassInd.setAdapter(adaptador);

        //associa um listener ao spinner
        spinnerClassInd.setOnItemSelectedListener(onClassificacaoIndicativaSelect());
    }

    private AdapterView.OnItemSelectedListener onClassificacaoIndicativaSelect() {
        return new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                imageClassInd.setImageResource(classIndArray[position]);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        };
    }

    //data de lançamento
    private void setupSpinnerData() {
        textDataLancamento = (TextView) findViewById(R.id.filme_data_lancamento);

        //data atual
        Calendar calendar = Calendar.getInstance();
        dia = calendar.get(Calendar.DAY_OF_MONTH);
        mes = calendar.get(Calendar.MONTH);
        ano = calendar.get(Calendar.YEAR);

        //coloca o dato no widget
        textDataLancamento.setText(String.format(locale, "%02d/%02d/%04d", dia, mes + 1, ano));

        //listener para selecionar a data
        textDataLancamento.setOnClickListener(onClickData());
    }

    //listener para a data de lançamento
     private View.OnClickListener onClickData() {

         return new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 //abre o datepickerdialog
                 datePickerDialog().show();

             }
         };
    }

    //método que cria o DatePickerDialog
    private DatePickerDialog datePickerDialog(){
        Calendar calendar = Calendar.getInstance();
        dia = calendar.get(Calendar.DAY_OF_MONTH);
        mes = calendar.get(Calendar.MONTH);
        ano = calendar.get(Calendar.YEAR);


        return new DatePickerDialog(this,datePickerListener(),ano,mes,dia);
    }

    private DatePickerDialog.OnDateSetListener datePickerListener(){
        return new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

                dia = dayOfMonth;
                mes = month;
                ano = year;

                //define o texto do textview
                textDataLancamento.setText(String.format(locale, "%02d/%02d/%04d", dia, mes + 1, ano));
            }
        };
    }

    //evento de click no imageView da capa do filme

    public void setCapaFilme(View view){
        // pergunta se a imagem vem da camera ou da galeria em um alertDialog
        fotoDialog().show();
    }

    private AlertDialog fotoDialog(){
        CharSequence[] opcoes = new CharSequence[] {
                getString(R.string.camera),
                getString(R.string.galeria)

        };
        //construir o alertDialog
        //cria uma instancia de builder
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle(getString(R.string.opcoes_imagem));
        builder.setItems(opcoes,onImageSelectListener());
        builder.setIcon(android.R.drawable.ic_menu_camera);
        //retorna a criação do alertDialog
        return builder.create();
    }

    //listener do aLErtDialog
    private DialogInterface.OnClickListener onImageSelectListener(){
        return new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                switch (which){
                    case 0:
                        //camera

                        dispatcherTakePicture();
                        break;

                    case 1:
                        if(PermissionUtils.isValidate(SaveFilmeActivity.this, GALERA_REQUEST_CODE,permissoes));
                        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(intent,GALERA_REQUEST_CODE);
                }
            }
        };
    }

    private void dispatcherTakePicture(){
        //verifica as permissões
        if(PermissionUtils.isValidate(this, CAMERA_REQUEST_CODE,permissoes)){
            //intent para acessar a camera
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

            //determina a melhor forma executar a intent
            if(takePictureIntent.resolveActivity(getPackageManager()) != null){
                //prepara o arquivo de foto
                photoFile = null;
                photoFile = createImage();
                if(photoFile != null){
                    //determina o caminho para salvar a foto
                    Uri uri = FileProvider.getUriForFile(getBaseContext(),"com.example.android.fileprovider",photoFile);
                    //inicia a activity para exibiar a foto no ImageView
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
                    startActivityForResult(takePictureIntent, CAMERA_REQUEST_CODE);
                }
            }
        }
    }

    private File createImage(){
        String name = System.currentTimeMillis() + ".jpg" ;

        File diretorio = getExternalFilesDir(Environment.DIRECTORY_PICTURES);

        String caminho = diretorio.getPath() + "/" + name;

        File imageFile = new File(caminho);
        return  imageFile;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        Bitmap bitmap = null;
        if(requestCode == CAMERA_REQUEST_CODE && resultCode == RESULT_OK){
            bitmap = BitmapFactory.decodeFile(photoFile.getAbsolutePath());
        }
        if(requestCode == GALERA_REQUEST_CODE && resultCode == RESULT_OK){
            if(data !=null){
                Uri uri = data.getData();
                // Nome do diretorio ond está a imagem
                String [] filePathColumn = {MediaStore.Images.Media.DATA};
                Cursor cursor = getContentResolver().query(uri,filePathColumn,null,null,null);

                //move o cursor para a primeira posição
                cursor.moveToFirst();

                //obtem o id da coljuna  0 (caminho da imagem)

                int columIndex = cursor.getColumnIndex(filePathColumn[0]);

                String caminho = cursor.getString(columIndex);

                //fecha o cursor

                cursor.close();

                //cria um File com o caminho selecionado
                photoFile = new File(caminho);
                bitmap = BitmapFactory.decodeFile(photoFile.getAbsolutePath());

            }
        }
        imgCapaFilme.setImageBitmap(bitmap);


    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);


        if(photoFile != null){
            outState.putString("foto",photoFile.getAbsolutePath());
        }
    }

    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        if (savedInstanceState != null) {

            //cria um objeto diretor
            filme = new Filme();

            //define os atributos de diretor
            //recuperando os valores da aplicaçãpo

            filme.setCapa(savedInstanceState.getString("foto"));



            //foto
            if (filme.getCapa() != null) {
                photoFile = new File(filme.getCapa());

                //bitmap
                Bitmap bitmap = BitmapFactory.decodeFile(photoFile.getAbsolutePath());

                if (bitmap != null) {
                    imgCapaFilme.setImageBitmap(bitmap);
                } else {
                    imgCapaFilme.setImageResource(R.drawable.noimage);

                }

            }
        }
    }


    private void setupSpinnerGeneros(){
        spinnerGeneros = (Spinner) findViewById(R.id.filme_genero_spinner);
        GeneroDao generoDao = new GeneroDao(this);

        //adaptador
        ArrayAdapter<Genero> adaptador = new ArrayAdapter<Genero>(this,android.R.layout.simple_spinner_dropdown_item, generoDao.findAll());

        //associa o spinner ao adaptador
        spinnerGeneros.setAdapter(adaptador);

    }

    private  void setupSpinnerDiretores(){
        spinnerDiretores = (Spinner) findViewById(R.id.filme_diretor_spinner);

        DiretorDao diretorDao = new DiretorDao(this);


        ArrayAdapter<Diretor> adaptador = new ArrayAdapter<Diretor>(this, android.R.layout.simple_spinner_dropdown_item, diretorDao.findAll());

        spinnerDiretores.setAdapter(adaptador);
    }


    public void saveFilmeOnClick(View view){
        if(filme==null) {
            filme = new Filme();
        }
            filme.setTitulo((editTitulo.getEditableText().toString()));
            filme.setSinopse(editSinopse.getEditableText().toString());
            filme.setTempoDuracao(Integer.parseInt(editDuracao.getEditableText().toString()));


            //recupera o valor do spinner
            ClassificacaoIndicativa classificacaoIndicativa = (ClassificacaoIndicativa) spinnerClassInd.getSelectedItem();
            filme.setClassificacaoIndicativa(classificacaoIndicativa);

            //capa do filme

            if(photoFile !=null){
                filme.setCapa(photoFile.getAbsolutePath());
            }

            //data de lançamento
            Calendar dataLancamento = Calendar.getInstance();
            dataLancamento.set(ano, mes , dia);
            filme.setDataLancamento(dataLancamento);

            //obtem um genero do spinner
            Genero genero = (Genero) spinnerGeneros.getSelectedItem();
            filme.setGenero(genero);

            Diretor diretor = (Diretor) spinnerDiretores.getSelectedItem();
            filme.setDiretor(diretor);

            filme.setNota(ratingNota.getRating());



        //realiza o cadastro do filme no banco de dados  ou não
        try {
            if (filmeDao.save(filme) != -1){
                MessageUtils.Toast(this,getString(R.string.sucesso_salvar_filme));

                clear();
            }else{
                MessageUtils.Toast(this,getString(R.string.erro_salvar_filme));
            }
        }catch (SQLException e){
            MessageUtils.Toast(this,getString(R.string.filme_ja_cadastrado));

        }

    }

    private void clear(){
        editTitulo.setText("");
        editSinopse.setText("");
        editDuracao.setText("");
        setupSpinnerClassInd();
        setupSpinnerDiretores();
        setupSpinnerGeneros();
        imgCapaFilme.setImageResource(R.drawable.noimage);
        ratingNota.setRating(0);
        editTitulo.requestFocus();
    }




    // --------------ate aq funciona so nao retorna para retornar
}

