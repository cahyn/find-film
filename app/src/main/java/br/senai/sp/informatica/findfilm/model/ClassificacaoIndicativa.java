package br.senai.sp.informatica.findfilm.model;

/**
 * Created by Tecnico_Tarde on 15/05/2017.
 */

public enum ClassificacaoIndicativa {

    LIVRE("Livre"),
    DEZ_ANOS("Dez Anos"),
    DOZE_ANOS("Doze Anos"),
    QUATORZE_ANOS("Quatorze Anos"),
    DEZESSEIS_ANOS("Dezesseis Anos"),
    DEZOITO_ANOS("Dezoito Anos");

    //atributo
    public String nome;

    //construtor
    ClassificacaoIndicativa(String nome){
        this.nome = nome;

    }

    @Override
    public String toString() {
        return this.nome;
    }
}
