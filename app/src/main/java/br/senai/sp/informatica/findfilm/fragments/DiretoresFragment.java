package br.senai.sp.informatica.findfilm.fragments;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

import br.senai.sp.informatica.findfilm.activity.SaveDiretoresActivity;
import br.senai.sp.informatica.findfilm.util.MessageUtils;
import br.senai.sp.informatica.findfilm.R;
import br.senai.sp.informatica.findfilm.adapter.RVDiretoresAdapter;
import br.senai.sp.informatica.findfilm.dao.DiretorDao;
import br.senai.sp.informatica.findfilm.model.Diretor;

/**
 * A simple {@link Fragment} subclass.
 */
public class DiretoresFragment extends Fragment {

    RecyclerView recyclerView;
    DiretorDao diretorDao;
    List<Diretor> diretores = new ArrayList<>();
    Diretor diretor;

    Context context;
    View view;




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //
        view = inflater.inflate(R.layout.fragment_diretores,container,false);
        //cria o recyclerview
        createlist();
        return view;
    }

    //cria a lista de diretores
    private void createlist(){
        recyclerView =(RecyclerView) view.findViewById(R.id.recycler_diretores);
        recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));

        //todos os itens do recyclerview tem o mesmo tamanho
        recyclerView.setHasFixedSize(true);

        //associa o recyclerview ao adaptador
        recyclerView.setAdapter(new RVDiretoresAdapter(diretores,getContext(),onClickDiretor() ) );
    }


    //inicia lista com os dados
    private void initDataSet(){
        diretorDao = new DiretorDao(getContext());
        //instancia uma dao de diretor popula a lista de diretores
        diretores = diretorDao.findAll();
    }

    //listener de onClick
    // é invocado quando o usuario clica em um item do recyclerview
    private RVDiretoresAdapter.DiretorOnClickListener onClickDiretor(){
        return new RVDiretoresAdapter.DiretorOnClickListener() {
            @Override
            public void onClickDiretor(View view, int position) {
                //Toast.makeText(getContext(), "CLICK", Toast.LENGTH_SHORT).show();
                diretor = diretores.get(position);

                //associa o menu de contexto ao recyclerview
                registerForContextMenu(recyclerView);
                //abre o menu de context
                getActivity().openContextMenu(view);
            }
        };


    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //popula alista do recyclerview
        initDataSet();
        //informa ao fragment que ele possui um menu de opções
        setHasOptionsMenu(true);

    }

    //atualzia a lista
    //apos um novo cadastro
    //o onresume é invocado sempre  que voce volta a lista
    @Override
    public void onResume() {
        super.onResume();
        initDataSet();
        createlist();
    }

    //menu de opções


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        //infla o menu
        inflater.inflate(R.menu.search_menu,menu);
        //obtem um item do menu
        MenuItem item = menu.findItem(R.id.search);

        //cria uma instancia SearchView
        //DO SUPPORT V7
        SearchView  searchView =  (SearchView) item.getActionView();
    //substitui o SearchView por um SearchAutoComplete
        SearchView.SearchAutoComplete searchAutoComplete = (SearchView.SearchAutoComplete)searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);

        //troca as cores do searchView
        searchAutoComplete.setHintTextColor(Color.WHITE);
        searchAutoComplete.setTextColor(Color.WHITE);

        //associa um listener ao searchView

        searchView.setOnQueryTextListener(onSearch());
        }

    private SearchView.OnQueryTextListener onSearch(){

            return  new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String newText) {
                    //alterar os valores da lista de diretores
                    initDataSet(newText);
                    //recria a lista de diretores
                    createlist();
                    return false;
                }
            };
        }

    private void initDataSet(String query){
            diretorDao = new DiretorDao(getContext());
            diretores = diretorDao.find(query);
        }

        //menu de contexto

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        //infla o menu de contexto

        MenuInflater inflater = getActivity().getMenuInflater();
        inflater.inflate(R.menu.context_menu,menu);
        menu.setHeaderIcon(android.R.drawable.ic_menu_sort_by_size);
        menu.setHeaderTitle(getString(R.string.opcoes_contexto));
    }


    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if (getUserVisibleHint()){
        switch (item.getItemId()) {
            case R.id.editar:

                //cria uma intent
                Intent intent = new Intent(getContext(), SaveDiretoresActivity.class);

                //passa o diretor como um parcelable extra
                intent.putExtra("diretor", Parcels.wrap(diretor));
                //inicia a activity
                startActivity(intent);
                return true;
            case R.id.excluir:
                deleteDialog().show();

                return true;
            default:
                return super.onContextItemSelected(item);
        }}
        return false;
    }

    //mostra uma alertDialog para confirmar  a exclusão
    private AlertDialog deleteDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext()).setTitle(getString(R.string.excluir_filme
        )).setMessage(getString(R.string.msg_excluir_filme))
                .setPositiveButton(getString(R.string.sim), onDeleteListener())
                .setNegativeButton(getString(R.string.nao), onDeleteListener())
                .setIcon(android.R.drawable.ic_menu_delete);
        return builder.create();
    }

    //importar o do dialog
    private DialogInterface.OnClickListener onDeleteListener(){
        return new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        if (diretorDao.delete(diretor) != -1){
                            MessageUtils.Toast(getContext(),getString(R.string.sucesso_exclur_diretores));
                            //atualzia a lista
                            initDataSet();
                            //recria a lista
                            createlist();
                        }else {
                            MessageUtils.Toast(getContext(),getString(R.string.erro_excluir_diretores));
                        }
                        break;
                    case DialogInterface.BUTTON_NEGATIVE:
                        dialog.dismiss();
                        break;

                }
            }
        };
    }
}
