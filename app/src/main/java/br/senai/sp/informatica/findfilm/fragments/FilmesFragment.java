package br.senai.sp.informatica.findfilm.fragments;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

import br.senai.sp.informatica.findfilm.R;
import br.senai.sp.informatica.findfilm.activity.SaveFilmeActivity;
import br.senai.sp.informatica.findfilm.adapter.RVFilmesAdapter;
import br.senai.sp.informatica.findfilm.dao.FilmeDao;
import br.senai.sp.informatica.findfilm.model.Filme;
import br.senai.sp.informatica.findfilm.util.MessageUtils;

/**
 * A simple {@link Fragment} subclass.
 */
public class FilmesFragment extends Fragment {

    RecyclerView recyclerFilmes;
    FilmeDao filmeDao;
    List<Filme> filmes = new ArrayList<>();
    Context context;
    Filme filme;
    View view;

    private void loadList(){
        recyclerFilmes = (RecyclerView) view.findViewById(R.id.recycler_filmes);
        //define o layout do recyclerView
        recyclerFilmes.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerFilmes.setHasFixedSize(true);
        //cria um adaptador para os dados
        RVFilmesAdapter adaptador = new RVFilmesAdapter(getContext(),filmes, onClickFilme());
        //associa o recyclerView ao adaptador
        recyclerFilmes.setAdapter(adaptador);

    }
    //popula a lista
    private void initDataSet(){

        filmeDao = new FilmeDao(getContext());
        filmes = filmeDao.findAll();
    }

    private void initDataSet(String query){
        filmeDao = new FilmeDao(getContext());
        filmes = filmeDao.find(query);
    }

    private RVFilmesAdapter.FilmesOnClickListener onClickFilme(){
        return new RVFilmesAdapter.FilmesOnClickListener() {
            @Override
            public void OnClickFilme(View view, int position) {
                filme = filmes.get(position);

                registerForContextMenu(recyclerFilmes);

                getActivity().openContextMenu(view);
            }

        };
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
         view = inflater.inflate(R.layout.fragment_filmes, container, false);
        loadList();
        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initDataSet();
        setHasOptionsMenu(true);
    }

    @Override
    public void onResume() {
        super.onResume();
        initDataSet();
        loadList();
    }


//---------------------------------------------------------------/

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        MenuInflater inflater = getActivity().getMenuInflater();
        inflater.inflate(R.menu.context_menu,menu);
        menu.setHeaderIcon(android.R.drawable.ic_menu_sort_by_size);
        menu.setHeaderTitle(getString(R.string.opcoes_contexto));
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if (getUserVisibleHint()){
            switch (item.getItemId()){
                case R.id.editar:

                    Intent intent = new Intent(getContext(), SaveFilmeActivity.class);

                    intent.putExtra("filme", Parcels.wrap(filme));
                    startActivity(intent);
                    return  true;
                case R.id.excluir:
                    deleteDialog().show();
                    return true;
                default:
                    return  super.onContextItemSelected(item);
            }
        }
        return false;
    }


    private AlertDialog deleteDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext()).setTitle(getString(R.string.titulo_excluir
        )).setMessage(getString(R.string.msg_excluir))
                .setPositiveButton(getString(R.string.sim), onDeleteListener())
                .setNegativeButton(getString(R.string.nao), onDeleteListener())
                .setIcon(android.R.drawable.ic_menu_delete);
        return builder.create();
    }

    private DialogInterface.OnClickListener onDeleteListener(){
        return new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        if (filmeDao.delete(filme) != -1){
                            MessageUtils.Toast(getContext(),getString(R.string.sucesso_exclur_filmes));
                            //atualzia a lista
                            initDataSet();
                            //recria a lista
                            loadList();
                        }else {
                            MessageUtils.Toast(getContext(),getString(R.string.erro_excluir_filmes));
                        }
                        break;
                    case DialogInterface.BUTTON_NEGATIVE:
                        dialog.dismiss();
                        break;

                }
            }
        };


    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        //infla o menu
        inflater.inflate(R.menu.search_menu,menu);
        //obtem um item do menu
        MenuItem item = menu.findItem(R.id.search);

        //cria uma instancia SearchView
        //DO SUPPORT V7
        SearchView  searchView =  (SearchView) item.getActionView();
        //substitui o SearchView por um SearchAutoComplete
        SearchView.SearchAutoComplete searchAutoComplete = (SearchView.SearchAutoComplete)searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);

        //troca as cores do searchView
        searchAutoComplete.setHintTextColor(Color.WHITE);
        searchAutoComplete.setTextColor(Color.WHITE);

        //associa um listener ao searchView

        searchView.setOnQueryTextListener(onSearch());
    }

    private SearchView.OnQueryTextListener onSearch(){

        return  new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                //alterar os valores da lista de diretores
                initDataSet(newText);
                //recria a lista de diretores
                loadList();
                return false;
            }
        };
    }
}
