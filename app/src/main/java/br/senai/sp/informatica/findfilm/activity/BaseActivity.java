package br.senai.sp.informatica.findfilm.activity;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import br.senai.sp.informatica.findfilm.R;

/**
 * Created by Tecnico_Tarde on 13/04/2017.
 */

public abstract class BaseActivity extends AppCompatActivity{
    /**
     * Método para Instanciar os widgets
     */
    public abstract void inicializarComponentes();



    /**
     * Método que substitui a ActionBar pela Toolbar
     */
    public void setupToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if(toolbar != null){
            //substitui a actionBar pela Toolbar
            setSupportActionBar(toolbar);
        }
    }
}
