package br.senai.sp.informatica.findfilm.model;

import org.parceler.Parcel;

/**
 * Created by Tecnico_Tarde on 17/04/2017.
 */
@Parcel
public class Genero {
    private long id;
    private String nome;
    private float classificacao;

    public Genero() {
    }

    public Genero(long id, String nome, float classificacao) {
        this.id = id;
        this.nome = nome;
        this.classificacao = classificacao;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public float getClassificacao() {
        return classificacao;
    }

    public void setClassificacao(float classificacao) {
        this.classificacao = classificacao;
    }

    @Override
    public String toString() {
        return this.nome;
    }
}
