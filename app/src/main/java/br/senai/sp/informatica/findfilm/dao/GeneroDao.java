package br.senai.sp.informatica.findfilm.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import br.senai.sp.informatica.findfilm.model.Genero;

/**
 * Created by Tecnico_Tarde on 17/04/2017.
 */

public class GeneroDao implements DefaultDao<Genero> {

    private SQLiteDatabase sqLiteDatabase;
    private DBHelper dbHelper;
    private Context context;


    // Bob Construtor
    public GeneroDao(Context context) {
        this.context = context;
        this.dbHelper = new DBHelper(context);
    }


    @Override
    public long save(Genero genero) {
        if (sqLiteDatabase == null) {
            //se assim for cria uma conexão com banco de dados
            //abre o arquivo de banco de dados com permissões de escrita
            sqLiteDatabase = dbHelper.getWritableDatabase();
        }
        try {
            ContentValues values = new ContentValues();
            //associa os valores do objeto diretor as colunas do banco dedados
            values.put("nome", genero.getNome());
            values.put("classificacao", genero.getClassificacao());
            if (genero.getId() != 0) {
                //update
                String[] whereArgs = new String[]{
                        String.valueOf(genero.getId())
                };
                return sqLiteDatabase.update("genero", values, "id=?", whereArgs);
            } else {
                return sqLiteDatabase.insertOrThrow("genero", null, values);
            }
        } finally {
            sqLiteDatabase.close();
        }
    }

    @Override
    public List<Genero> findAll() {
        if (sqLiteDatabase == null) {
            sqLiteDatabase = dbHelper.getWritableDatabase();
        }
        try {
            String sql = "SELECT * FROM genero  ORDER BY classificacao DESC";
            Cursor cursor = sqLiteDatabase.rawQuery(sql, null);
            return toList(cursor);
        } finally {
            sqLiteDatabase.close();
        }
    }

    @Override
    public List<Genero> find(String query) {
        if (sqLiteDatabase ==null){
            sqLiteDatabase = dbHelper.getWritableDatabase();
        }
        try {
            String[] parametro =
                    new String[]{"%" + query + "%"}  ;
            String sql = "SELECT * FROM genero WHERE nome LIKE ? ORDER BY classificacao DESC";
            Cursor cursor = sqLiteDatabase.rawQuery(sql,parametro);

            return toList(cursor);
        }finally {
            sqLiteDatabase.close();
        }
    }

    @Override
    public long delete(Genero genero) {
        if (!sqLiteDatabase.isOpen() || sqLiteDatabase == null) {
            sqLiteDatabase = dbHelper.getWritableDatabase();

        }
        try {
            String[] whereArgs = new String[]{String.valueOf(genero.getId())};
            return sqLiteDatabase.delete("genero", "id=?", whereArgs);
        } finally {
            sqLiteDatabase.close();
        }
    }

    @Override
    public List<Genero> toList(Cursor cursor) {
        List<Genero> generos = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do{
            Genero genero = new Genero();
            genero.setId(cursor.getLong(cursor.getColumnIndex("id")));
            genero.setNome(cursor.getString(cursor.getColumnIndex("nome")));
            genero.setClassificacao(cursor.getFloat(cursor.getColumnIndex("classificacao")));

            generos.add(genero);
        } while (cursor.moveToNext()) ;
    }
    return generos;
}
}
