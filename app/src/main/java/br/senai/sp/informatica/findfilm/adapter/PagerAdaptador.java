package br.senai.sp.informatica.findfilm.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import br.senai.sp.informatica.findfilm.fragments.DiretoresFragment;
import br.senai.sp.informatica.findfilm.fragments.FilmesFragment;
import br.senai.sp.informatica.findfilm.fragments.GenerosFragment;
import br.senai.sp.informatica.findfilm.fragments.SobreFragment;

/**
 * Created by Tecnico_Tarde on 17/04/2017.
 */

public class PagerAdaptador extends FragmentPagerAdapter {

    int numOfTabs;


    public PagerAdaptador(FragmentManager fm, int numOfTabs) {
        super(fm);
        this.numOfTabs = numOfTabs;

    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                //instancia o fragmente de filmes
                FilmesFragment filmesFragment = new FilmesFragment();

                //retorna o fragment de filmes
                return  filmesFragment;
            case 1:
                //instancia o fragmente de generos
                GenerosFragment generosFragment = new GenerosFragment();
                return generosFragment;

            case 2:
                DiretoresFragment diretoresFragment = new DiretoresFragment();
                return  diretoresFragment;
            case 3:
                SobreFragment sobreFragment = new SobreFragment();
                return sobreFragment;
            default:
                return null;

        }
    }

    @Override
    public int getCount() {
        return numOfTabs != 0 ? numOfTabs : 0;
    }
}
