package br.senai.sp.informatica.findfilm.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import br.senai.sp.informatica.findfilm.R;
import br.senai.sp.informatica.findfilm.adapter.PagerAdaptador;
import br.senai.sp.informatica.findfilm.fragments.AboutDialog;


public class MainActivity extends BaseActivity {

    TabLayout tabLayout;
    ViewPager viewPager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        inicializarComponentes();
        setupToolbar();
    }

    @Override
    public void inicializarComponentes() {
        //configura as trabs de tab layout

        setupTabs();
        setViewPager();

    }


    private void setupTabs(){
        tabLayout = (TabLayout) findViewById(R.id.tab_layout);

        //add as tabs

        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.filmes)));
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.generos)));
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.diretores)));
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.sobre)));


        //preenche completamente o espaço do tab layout
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);


        //lisaterner das tabs
        tabLayout.addOnTabSelectedListener(onTabSelected());
    }

    /**
     * Listener das tabs
     */
    private TabLayout.OnTabSelectedListener onTabSelected(){
        return new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
             //exibe o fragment no viwepager
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        };
    }

    private void setViewPager(){
        viewPager = (ViewPager) findViewById(R.id.viwe_pager);

        //associa o viwePager ao adaptador
        viewPager.setAdapter(new PagerAdaptador(getSupportFragmentManager(), tabLayout.getTabCount() ));



        //associa um listener ao viewpager  e altera a tab
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
    }



    public void addOnClick(View view){
        Intent intent;

        switch (view.getId()){
            case R.id.fab_diretores:
                intent = new Intent(this, SaveDiretoresActivity.class);
                startActivity(intent);
                break;
            case R.id.fab_generos:
                intent = new Intent(this, SaveGenerosActivity.class);
                startActivity(intent);
                break;
            case R.id.fab_filmes:
                intent = new Intent(this, SaveFilmeActivity.class);
                startActivity(intent);
                break;

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //infla o menu
        MenuInflater inflater = new MenuInflater(this);
        inflater.inflate(R.menu.about_menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    //trata os eventos do menu de opções

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_sobre:
                //exibe o dialog
                AboutDialog.showDialog(getSupportFragmentManager());
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
