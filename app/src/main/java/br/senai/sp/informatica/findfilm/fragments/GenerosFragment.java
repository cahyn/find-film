package br.senai.sp.informatica.findfilm.fragments;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

import br.senai.sp.informatica.findfilm.R;
import br.senai.sp.informatica.findfilm.activity.SaveGenerosActivity;
import br.senai.sp.informatica.findfilm.adapter.RVGenerosAdapter;
import br.senai.sp.informatica.findfilm.dao.GeneroDao;
import br.senai.sp.informatica.findfilm.model.Genero;
import br.senai.sp.informatica.findfilm.util.MessageUtils;

/**
 * A simple {@link Fragment} subclass.
 */
public class GenerosFragment extends Fragment {

    RecyclerView recyclerView;
    GeneroDao generoDao;
    List<Genero> generos = new ArrayList<>();
    Genero genero;

    Context context;
    View view;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view= inflater.inflate(R.layout.fragment_generos, container, false);
        //cria o recyclerview C
        createlist();
        return view;
    }

    //cria a lista de diretores
    private  void createlist(){
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_generos);
        recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));

        //todos os itens do recyclerview tem o mesmo tamanho
        recyclerView.setHasFixedSize(true);

        //associa o recyclerview ao adaptador
        recyclerView.setAdapter(new RVGenerosAdapter(generos,getContext(), onClickGenero() ));
    }

    private void initDataSet(){
        generoDao = new GeneroDao(getContext());
        //instancia uma dao de genero e popula a lista
        generos = generoDao.findAll();
    }

    //listener de onClick
    // e é invocado quando o usuario clica e um recyclerview
    private RVGenerosAdapter.GenerosOnClickListener onClickGenero(){
       return new RVGenerosAdapter.GenerosOnClickListener() {
           @Override
           public void onClickGenero(View view, int position) {
               //toast
               genero = generos.get(position);

               //associa ao menu de contexto ao recycleview
               registerForContextMenu(recyclerView);
               //abre o menu de context
               getActivity().openContextMenu(view);
           }
       } ;
    }

    public void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        //popular a lista do recyclerView
        initDataSet();
        //informa ao fragment que ele possui um menu de opções
        setHasOptionsMenu(true);
    }

    //atualzia a lista
    //apos um novo cadastro
    //o onresume é invocado sempre  que voce volta a lista
    @Override
    public void onResume() {
        super.onResume();
        initDataSet();
        createlist();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
    //infla o menu
        inflater.inflate(R.menu.search_menu,menu);

        MenuItem item = menu.findItem(R.id.search);

        //cria uma instancia serachView
        SearchView searchView = (SearchView) item.getActionView();
        //substitui o searchView por um searchAutocomplete
        SearchView.SearchAutoComplete searchAutoComplete = (SearchView.SearchAutoComplete)searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);

        //troca as cores do searchView
        searchAutoComplete.setHintTextColor(Color.WHITE);
        searchAutoComplete.setTextColor(Color.WHITE);

        //associa um listener ao searhVIEW
        searchView.setOnQueryTextListener(onSearch());
    }

    private SearchView.OnQueryTextListener onSearch(){
        return new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
               initDataSet(newText);
                createlist();
                return false;
            }
        };
    }

   private void initDataSet(String query){
       generoDao = new GeneroDao(getContext());
       generos = generoDao.find(query);
   }

    public  boolean onContextItemSelected(MenuItem item){
       if (getUserVisibleHint()) {
           switch (item.getItemId()) {
               case R.id.editar:
                   //cria uma intent
                   Intent intent = new Intent(getContext(), SaveGenerosActivity.class);
                    intent.putExtra("genero", Parcels.wrap(genero));
                   startActivity(intent);
                   return true;
               case R.id.excluir:
                   deleteDialog().show();
                   return true;
               default:
                   return super.onContextItemSelected(item);
           }
       }
    return false;
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        //infla o menu de contexto

        MenuInflater inflater = getActivity().getMenuInflater();
        inflater.inflate(R.menu.context_menu,menu);
        menu.setHeaderIcon(android.R.drawable.ic_menu_sort_by_size);

    }


    //mostra uma alertDialog para confirmar  a exclusão
    private AlertDialog deleteDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext()).setTitle(getString(R.string.titulo_excluir
        )).setMessage(getString(R.string.msg_excluir))
                .setPositiveButton(getString(R.string.sim), onDeleteListener())
                .setNegativeButton(getString(R.string.nao), onDeleteListener())
                .setIcon(android.R.drawable.ic_menu_delete);
        return builder.create();
    }

    private DialogInterface.OnClickListener onDeleteListener(){
        return new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                case DialogInterface.BUTTON_POSITIVE:
                    if (generoDao.delete(genero) != -1){
                        MessageUtils.Toast(getContext(),getString(R.string.sucesso_exclur_diretores));
                        //atualzia a lista
                        initDataSet();
                        //recria a lista
                        createlist();
                    }else{
                        MessageUtils.Toast(getContext(),getString(R.string.erro_excluir_diretores));
                    }
                break;
                case DialogInterface.BUTTON_NEGATIVE:
                dialog.dismiss();
                break;
                    }
            }
        };
    }
}
