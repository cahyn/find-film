package br.senai.sp.informatica.findfilm.util;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tecnico_Tarde on 24/04/2017.
 */

public class PermissionUtils {

    public static boolean isValidate(Activity activity, int requestCode, String... permissions){
        List<String> list = new ArrayList<>();

        for(String permission : permissions){
            //verifica se a permissão ainda  não foi solicitada
            if(!checkPermission(activity,permission)){
                //adiciona a permissão na lista
                list.add(permission);
            }

        }
        //se a lista estiver vazia não há mais permissoes para verificar
        if(list.isEmpty()){
            return true;


        }

        //o array q vai receber o arraylist
        String[] newPermissions = new String[list.size()];

        //se houver elementos na lista converte o arraylist em array



        list.toArray(newPermissions);

        //solicita a permissão para o usuario
        ActivityCompat.requestPermissions(activity,newPermissions, requestCode);

        //retorna falso pois podem haver mais permissões a verificar
        return  false;
    }

    private static boolean checkPermission(Context context,String permission){

        return ContextCompat.checkSelfPermission(context, permission) == PackageManager.PERMISSION_GRANTED;
    }
}
