package br.senai.sp.informatica.findfilm.dao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Tecnico_Tarde on 17/04/2017.
 */


public class DBHelper extends SQLiteOpenHelper{

    public static final String DB_NAME = "findFilm.db";

    public static  final int DB_VERSION = 1;

    public static final String TAG = "FINDFILM";


    public DBHelper(Context context) {
        super(context,DB_NAME,null, DB_VERSION);
    }

    //ddl das tabelas

    public static final String SQL_DIRETOR = "CREATE TABLE IF NOT EXISTS diretor(" +
            "id INTEGER PRIMARY KEY AUTOINCREMENT," +
            "nome TEXT NOT NULL UNIQUE COLLATE NOCASE," +
            "foto TEXT," +
            "classificacao REAL );";


    public static  final String SQL_GENERO = "CREATE TABLE IF NOT EXISTS genero(" +
            "id INTEGER PRIMARY KEY AUTOINCREMENT," +
            "nome TEXT NOT NULL UNIQUE COLLATE NOCASE," +
            "classificacao REAL);";

    public static final String SQL_FILME = "CREATE TABLE IF NOT EXISTS filme(" +
            "id INTEGER PRIMARY KEY AUTOINCREMENT," +
            "titulo TEXT NOT NULL UNIQUE COLLATE NOCASE," +
            "sinopse TEXT," +
            "duracao NUMERIC," +
            "classificacaoIndicativa TEXT NOT NULL," +
            "capa TEXT," +
            "dataLancamento NUMERIC NOT NULL," +
            "genero_id INTEGER NOT NULL," +
            "diretor_id INTEGER NOT NULL," +
            "nota REAL," +
            "FOREIGN KEY (genero_id) REFERENCES genero(id)," +
            "FOREIGN KEY (diretor_id) REFERENCES diretor(id)" +
            ");";
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_DIRETOR);
        db.execSQL(SQL_GENERO);
        db.execSQL(SQL_FILME);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);

        if(!db.isReadOnly()){
            db.execSQL("PRAGMA foreign_keys=ON;");
        }
    }
}

