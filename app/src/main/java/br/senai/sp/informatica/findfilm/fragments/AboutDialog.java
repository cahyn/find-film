package br.senai.sp.informatica.findfilm.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.widget.TextView;

import br.senai.sp.informatica.findfilm.R;

/**
 * Created by Tecnico_Tarde on 25/05/2017.
 */

public class AboutDialog extends DialogFragment{

    //IMPORTA TUDO DAPORRA DO SUPPORT V4
    /**
     * Obtém a versão da aplicação.
     * @param activity A activity utilizada para obtem um packegermanager
     * @return retornar um versionNome (versão do aplicativo)
     */
    private String  gerVersionName(Activity activity){
        //obtém uma instãncia de packagerManager
        PackageManager manager = activity.getPackageManager();

        String packageName = activity.getPackageName();

        //versão da aplicação

        String versionName;
        try{
            //ob´tem informações sobre o pacote
            PackageInfo info = manager.getPackageInfo(packageName,0);
            //obtem o version name da aplicação
            // app/build.gradle
            versionName = info.versionName;


        }catch (PackageManager.NameNotFoundException e){
    versionName = "N/A";
        }
        return  versionName;
    }

    /**
     * Exibe um dialog
     * @param fragmentManager gerencia a transação de fragments.
     */
    public static void showDialog(FragmentManager fragmentManager){
        //inicia uma transação de fragments
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        Fragment fragment = fragmentManager.findFragmentByTag("about_dialog");

        if (fragment !=null){
            //remove o fragment por meio de uma transação
            transaction.remove(fragment);
        }
        //adiciona null a back stack (deixa ela VAZIA )
        transaction.addToBackStack(null);

        //exibe o dialog
        new AboutDialog().show(transaction,"about_dialog");

    }

    //invoca O METODO SHOWDIALOG()


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // obtem o versionName
        String versionName = gerVersionName(getActivity());
        SpannableStringBuilder body = new SpannableStringBuilder();

        //se a api do dispositivo é >=24
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N){
            body.append(Html.fromHtml(getString(R.string.about_dialog_text,versionName, Html.FROM_HTML_MODE_COMPACT)));
        }else {
            body.append(Html.fromHtml(getString(R.string.about_dialog_text,versionName)));
        }
        //infla o layour do dialog
        LayoutInflater inflater =  LayoutInflater.from(getActivity());
        TextView view = (TextView) inflater.inflate(R.layout.about_dialog, null);
        view.setText(body);

        //cria o link
        view.setMovementMethod(new LinkMovementMethod());
        //retorna o dialog
        return  new AlertDialog.Builder(getActivity())
                .setTitle(getString(R.string.about_title))
                .setView(view)
                .setPositiveButton(getString(R.string.ok),okListener())
                .create();
    }


    //listener de positivebutton trata os eventos do positivebutton
    private DialogInterface.OnClickListener okListener() {
    return new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            dialog.dismiss();
        }

    };
}
}

