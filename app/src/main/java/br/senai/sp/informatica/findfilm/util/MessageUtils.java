package br.senai.sp.informatica.findfilm.util;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.Toast;

/**
 * Created by Tecnico_Tarde on 13/04/2017.
 */

public class MessageUtils {

    /**
     * Método que exibe mensagens para o usuário
     * @param context Contexto de execução
     * @param msg Mensagem a ser Exibida
     */
    public  static void Toast(Context context, String msg){
        Toast.makeText(context,msg,Toast.LENGTH_SHORT).show();
    }


    public static void snack(View view, CharSequence msg){
        Snackbar.make(view,msg,Snackbar.LENGTH_SHORT).show();

    }

}
