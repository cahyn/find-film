package br.senai.sp.informatica.findfilm.activity;

import android.database.sqlite.SQLiteConstraintException;
import android.os.Parcel;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;

import org.parceler.Parcels;

import br.senai.sp.informatica.findfilm.R;
import br.senai.sp.informatica.findfilm.dao.GeneroDao;
import br.senai.sp.informatica.findfilm.model.Genero;
import br.senai.sp.informatica.findfilm.util.MessageUtils;

public class SaveGenerosActivity extends BaseActivity {

    EditText editNomeGenero;
    RatingBar ratingClasGenero;
    CoordinatorLayout generosLayout;

    Genero genero = null;
    GeneroDao generoDao = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_save_generos);
        inicializarComponentes();
        setupToolbar();
    if (getIntent().getExtras() !=null){
        genero = Parcels.unwrap(getIntent().getParcelableExtra("genero"));
        if (genero !=null){
            editNomeGenero.setText(genero.getNome());
            ratingClasGenero.setRating(genero.getClassificacao());
        }
    }
    }

    @Override
    public void inicializarComponentes() {
        editNomeGenero = (EditText) findViewById(R.id.edit_genero);
        ratingClasGenero = (RatingBar) findViewById(R.id.rating_nota_genero);
        generosLayout = (CoordinatorLayout) findViewById(R.id.generos_layout);

    }

    @Override
    public void setupToolbar() {
        super.setupToolbar();
    }

    public void saveGeneroOnClick(View view){
        if(isValidated()) {
            try {
                if (genero == null) {
                    genero = new Genero();
                }
                //atribui os dados ao genero
                genero.setNome(editNomeGenero.getEditableText().toString());

                genero.setClassificacao(ratingClasGenero.getRating());

                generoDao = new GeneroDao(this);
                if (generoDao.save(genero) != -1) {
                    //sucesso ao salvar
                    MessageUtils.snack(generosLayout, getString(R.string.sucesso_salvar_generos));
                } else {
                    //erro
                    MessageUtils.snack(generosLayout, getString(R.string.erro_salvar_generos));

                }
                clear();
            } catch (SQLiteConstraintException e) {
                MessageUtils.snack(generosLayout, getString(R.string.genero_cadastrado));
            }
        }
    }

    private void clear(){
        editNomeGenero.setText("");
        ratingClasGenero.setRating(0);
        editNomeGenero.requestFocus();
        genero = null;
    }


    private boolean isValidated(){
        if(editNomeGenero.getEditableText().toString().isEmpty()) {
            MessageUtils.snack(generosLayout, getString(R.string.preencha_nome_genero));
            editNomeGenero.requestFocus();
            return false;
        }else if (ratingClasGenero.getRating()<0.5){
            MessageUtils.snack(generosLayout, getString(R.string.preencha_classificacao_genero));
            return false;
        }
        return true;

    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.d("TAG","onSaveInstanceState()");
        outState.putString("nome",editNomeGenero.getEditableText().toString());
        outState.putFloat("classificacao",ratingClasGenero.getRating());
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        if (savedInstanceState != null){
            //cria um objeto diretor
            genero = new Genero();

            genero.setNome(savedInstanceState.getString("nome"));
            genero.setClassificacao(savedInstanceState.getFloat("classificacao"));

        editNomeGenero.setText(genero.getNome());
        ratingClasGenero.setRating(genero.getClassificacao());
        }
    }

}
