package br.senai.sp.informatica.findfilm.dao;

import android.database.Cursor;

import java.util.List;

/**
 * Created by Tecnico_Tarde on 17/04/2017.
 */

public interface DefaultDao<T> {

    public long save(T t);

    public List<T> findAll();

    public List<T> find(String query);

    public long delete (T t);

    public List<T> toList(Cursor cursor);

}
